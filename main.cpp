#include "Obfuscate.h"
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //QTextStream cin(stdin);
    //QTextStream cout(stdout);

    QString path(*(++argv));
    //QString path2;

    //cout << "Set directory DB: \n";
    //cout.flush();
    //path2 = cin.readLine();

    Obfuscate obfuscate;
    if (!obfuscate.CreateConnection(path)) {
        return -1;
    }

    if(obfuscate.MaxOperatorid()){
       return 1;
    }

    if(!obfuscate.ChangLacCellid()){
       qDebug() << "Ok ChangLacCellid";
    }

    if(!obfuscate.InsertOperators()){
       qDebug() << "Ok InsertOPERATORS";
    }

    if(!obfuscate.NewId()){
      qDebug() << "Ok NewID";
    }

    if(!obfuscate.ChangCellnameAdrLatLonLacCellid()){
        qDebug() << "Ok ChangLacCidCellnameAdrLatLon";
    }

    return a.exec();
}
