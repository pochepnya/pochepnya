#include "Obfuscate.h"

bool Obfuscate::CreateConnection(const QString &str)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(str);
    //db.setUserName("elton");
    //db.setHostName("epica");
    //db.setPassword("password");
    if (!db.open()) {
        qDebug() << "Cannot open database:" << db.lastError();
        return false;
    }
    return true;
}

int Obfuscate::ChangCellnameAdrLatLonLacCellid()
{
    QSqlQuery query;
    //Заменил содержимое колонки CELL_NAME на LAC + CELLID и ADR поменял на Some Address, Переместил координаты LAT и LON в район Африки
    if (!query.exec("UPDATE CELLS set CELL_NAME =  CAST(LAC as TEXT) || cast(CELLID as TEXT), ADR = 'Some Address' "
                    ",LAT = (48.4699449530707 - LAT) + (-14.801281)"
                    ",LON = (cast(35.0102777777778 as DOUBLE PRECISION)- cast(LON as DOUBLE PRECISION)) + (25.565655);")) {
        qDebug() << "Unable to execute query - exiting";
        return 1;
    }
    return 0;
}

int Obfuscate::ChangLacCellid()
{
    QSqlQuery query;
    //Заменил содержимое колонки CELL_NAME на LAC + CELLID и ADR поменял на Some Address, Переместил координаты LAT и LON в район Африки
    if (!query.exec("UPDATE CELLS set LAC = abs(random() % (1000 - 0)), CELLID = abs(random() % (1000 - 0));")) {
        qDebug() << "Unable to execute query - exiting";
        return 1;
    }
    return 0;
}

int Obfuscate::InsertOperators()
{
    //Додовляю 4 новые записи в таблицу OPERATORS
    QSqlQuery query;
    int mnc = 90;
    int ID = maxID;
    QString strF =
          "INSERT INTO  OPERATORS (OPERATORID, MCC, MNC, NAME, FULLNAME) "
          "VALUES(%1, '%2', '%3', '%4', '%5');";

    QString str = strF.arg(++ID)
              .arg("999")
              .arg(++mnc)
              .arg("AQUA")
              .arg("AQUA");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert opeation";
    }

    str = strF.arg(++ID)
              .arg("999")
              .arg(++mnc)
              .arg("BRO")
              .arg("BRO");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert opeation";
    }

    str = strF.arg(++ID)
              .arg("999")
              .arg(++mnc)
              .arg("RED")
              .arg("RED");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert opeation";
    }

    str = strF.arg(++ID)
              .arg("999")
              .arg(++mnc)
              .arg("GREY")
              .arg("GREY");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert opeation";
    }
    return 0;
}

int Obfuscate::NewId()
{
    QSqlQuery query;
    QSqlQuery query2;
    int ID = maxID;
    //Заменяю id на новые id, которые я создам
    if (!query.exec("SELECT OPERATORID FROM CELLS GROUP by OPERATORID;")) {
        qDebug() << "Unable to execute query - exiting";
        return 1;
    }

    QSqlRecord rec2 = query.record();
    if(rec2.isEmpty())
    {
        qDebug() << "No record";
        return 1;
    }
    while (query.next()) {
        QString strin = "UPDATE CELLS set OPERATORID = %1  where OPERATORID = %2;";
        QString str = strin.arg(++ID)
                .arg(query.value(rec2.indexOf("OPERATORID")).toInt());
        query2.exec(str);
    }
    return 0;
}

int Obfuscate::MaxOperatorid()
{
    QSqlQuery query;
    //Нахожу максимальный id, который потом использую дальше.
    if (!query.exec("SELECT max(OPERATORID) as OPERATORID FROM OPERATORS;")) {
        qDebug() << "Unable to execute query - exiting";
        return 1;
    }

    QSqlRecord rec = query.record();
    if(rec.isEmpty())
    {
        qDebug() << "No record";
        return 1;
    }
    query.next();
    maxID  = query.value(rec.indexOf("OPERATORID")).toInt();

    return 0;
}

