#ifndef OBFYS_H
#define OBFYS_H

#include <QTextStream>
#include <QtSql>
#include <QString>

class Obfuscate
{
    int maxID = 0;
public:
    Obfuscate(){}
    static bool CreateConnection(const QString &str);
    int ChangCellnameAdrLatLonLacCellid();
    int ChangLacCellid();
    int InsertOperators();
    int NewId();
    int MaxOperatorid();
    ~Obfuscate(){}
};

#endif // OBFYS_H
